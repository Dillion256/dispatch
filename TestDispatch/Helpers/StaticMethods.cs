﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using TestDispatch.Models;

namespace TestDispatch.Helpers
{
    public static class StaticMethods
    {
        public static Double distanceFrom(Location firstLocation, Location secondLocation)
        {


            Dictionary<String, Double> deltas = getDeltas(firstLocation, secondLocation);
            return Math.Sqrt(Math.Pow(deltas["Latitude"], 2) + Math.Pow(deltas["Longitude"], 2));
        }


        public static Dictionary<String, Double> getDeltas(Location firstLocation, Location secondLocation)
        {
            Dictionary<String, Double> latitudeAndLongitude = new Dictionary<string, double>();

            if (firstLocation.Latitude_sign.Equals(secondLocation.Latitude_sign))
            {
                latitudeAndLongitude.Add("Latitude", Math.Abs(firstLocation.Latitude - secondLocation.Latitude));
            }
            else
            {
                latitudeAndLongitude.Add("Latitude", Math.Abs(firstLocation.Latitude + secondLocation.Latitude));
            }

            if (secondLocation.Longitude_sign.Equals(firstLocation.Longitude_sign))
            {
                latitudeAndLongitude.Add("Longitude", Math.Abs(firstLocation.Longitude - secondLocation.Longitude));
            }
            else
            {
                latitudeAndLongitude.Add("Longitude", Math.Abs(firstLocation.Longitude + secondLocation.Longitude));
            }

            return latitudeAndLongitude;
        }

        public static FareRequest getFarthestCustomer(Driver driver, List<FareRequest> customers)
        {
            customers.Sort(delegate(FareRequest fare1, FareRequest fare2) { return distanceFrom(driver.Location, fare1.Destination).CompareTo(distanceFrom(driver.Location, fare2.Destination)); });
            return customers[customers.Count - 1];
        }

        public static Double headingToDestination(Location startingLocation, Location destination)
        {
            Dictionary<String, Double> deltas = getDeltas(startingLocation, destination);
            return Math.Atan(deltas["Latitude"] / deltas["Longitude"]);
        }

        public static List<Customer> getMostRecentCustomers(List<Customer> customers)
        {
            List<Customer> returnList = new List<Customer>();

            foreach (Customer c in customers)
            {
                if (c.Location.LastUpdated != null)
                {

                    TimeSpan span = DateTime.Now - c.Location.LastUpdated.Value;
                    if (span.Minutes <= 60)
                    {
                        returnList.Add(c);
                    }
                }
            }

            return returnList;
        }

        public static List<Driver> getClosestDrivers(Location customerLocation, List<Driver> driverLocations)
        {
            driverLocations.Sort(delegate(Driver driver1, Driver driver2) { return distanceFrom(driver1.Location, customerLocation).CompareTo(distanceFrom(driver2.Location, customerLocation)); });
            //driverLocations = (List<DriverLocation>)driverLocations;
            foreach (Driver d in driverLocations)
            {
                Debug.WriteLine("Driver: " + d.UserID.ToString());
            }

            if (driverLocations.Count < 5)
            {
                return driverLocations.ToList();
            }

            return driverLocations.Take(5).ToList();
        }

        public static List<Customer> getClosestCustomers(Driver driver, List<Customer> customers)
        {
            customers.Sort(delegate(Customer customer1, Customer customer2) { return distanceFrom(driver.Location, customer1.Location).CompareTo(distanceFrom(driver.Location, customer2.Location)); });

            if (customers.Count < 5)
            {
                return customers.ToList();
            }

            return customers.Take(5).ToList();
        }

        public static List<FareRequest> getClosestFares(Driver driver, List<FareRequest> customers)
        {
            customers.Sort(delegate(FareRequest customer1, FareRequest customer2) { return distanceFrom(driver.Location, customer1.Customer.Location).CompareTo(distanceFrom(driver.Location, customer2.Customer.Location)); });

            if (customers.Count < 5)
            {
                return customers.ToList();
            }

            return customers.Take(5).ToList();
        }

        public static List<FareRequest> getCustomersByHeading(Driver driver, Location driverDestination, List<FareRequest> customers)
        {
            customers.Sort(delegate(FareRequest customer1, FareRequest customer2) { return Math.Abs(headingToDestination(driver.Location, driverDestination) - headingToDestination(customer1.Customer.Location, customer1.Destination)).CompareTo(headingToDestination(driver.Location, driverDestination) - headingToDestination(customer2.Customer.Location, customer2.Destination)); });

            if (customers.Count < 5)
            {
                return customers.ToList();
            }

            return customers.Take(5).ToList();
        }

        
        public static Boolean checkIfCurrent(DateTime? lastModified)
        {
            if (lastModified == null)
            {
                return false;
            }

            TimeSpan span = DateTime.Now - lastModified.Value;
            if (span.Minutes > 60)
            {
                return false;
            }

            return true;
        }

        public static List<Customer> temporaryTestCustomers()
        {
            Customer ones = new Customer();
            ones.deviceID = "12222";
            ones.Location = new Location();
            ones.Location.Latitude = 1.222222;
            ones.Location.Latitude_sign = "+";
            ones.Location.Longitude = 1.222222;
            ones.Location.Longitude_sign = "+";
            ones.PhoneNumber = "1111111";
            ones.Email = "ones@one.com";
            Customer twos = new Customer();
            twos.deviceID = "23333";
            twos.Location = new Location();
            twos.Location.Latitude = 2.33333;
            twos.Location.Latitude_sign = "+";
            twos.Location.Longitude = 2.33333;
            twos.Location.Longitude_sign = "+";
            twos.PhoneNumber = "2222222";
            twos.Email = "twos@twos.com";
            Customer thre = new Customer();
            thre.deviceID = "34444";
            thre.Location = new Location();
            thre.Location.Latitude = 3.444444;
            thre.Location.Latitude_sign = "+";
            thre.Location.Longitude = 3.444444;
            thre.Location.Longitude_sign = "+";
            thre.PhoneNumber = "3333333";
            thre.Email = "thre@thre.com";
            Customer four = new Customer();
            four.deviceID = "45555";
            four.Location = new Location();
            four.Location.Latitude = 4.555555;
            four.Location.Latitude_sign = "+";
            four.Location.Longitude = 4.555555;
            four.Location.Longitude_sign = "+";
            four.PhoneNumber = "4444444";
            four.Email = "four@four.com";
            Customer five = new Customer();
            five.deviceID = "56666";
            five.Location = new Location();
            five.Location.Latitude = 5.666666;
            five.Location.Latitude_sign = "+";
            five.Location.Longitude = 5.666666;
            five.Location.Longitude_sign = "+";
            five.PhoneNumber = "5555555";
            five.Email = "five@five.com";

            List<Customer> customers = new List<Customer>()
            {
                ones,
                twos,
                thre,
                four,
                five
            };

            foreach (Customer c in customers)
            {
                c.Location.LastUpdated = DateTime.Now;
            }

            return customers;
        }

        public static List<Driver> temporaryTestDrivers()
        {
            Driver ones = new Driver();
            ones.deviceID = "11111";
            ones.Location = new Location();
            ones.Location.Latitude = 1.1111111;
            ones.Location.Latitude_sign = "+";
            ones.Location.Longitude = 1.1111111;
            ones.Location.Longitude_sign = "+";
            Driver twos = new Driver();
            twos.deviceID = "22222";
            twos.Location = new Location();
            twos.Location.Latitude = 2.22222;
            twos.Location.Latitude_sign = "+";
            twos.Location.Longitude = 2.22222;
            twos.Location.Longitude_sign = "+";
            Driver thre = new Driver();
            thre.deviceID = "33333";
            thre.Location = new Location();
            thre.Location.Latitude = 3.333333;
            thre.Location.Latitude_sign = "+";
            thre.Location.Longitude = 3.333333;
            thre.Location.Longitude_sign = "+";
            Driver four = new Driver();
            four.deviceID = "44444";
            four.Location = new Location();
            four.Location.Latitude = 4.444444;
            four.Location.Latitude_sign = "+";
            four.Location.Longitude = 4.444444;
            four.Location.Longitude_sign = "+";
            Driver five = new Driver();
            five.deviceID = "55555";
            five.Location = new Location();
            five.Location.Latitude = 5.555555;
            five.Location.Latitude_sign = "+";
            five.Location.Longitude = 5.555555;
            five.Location.Longitude_sign = "+";

            List<Driver> drivers = new List<Driver>()
            {
                ones,
                twos,
                thre,
                four,
                five
            };

            foreach (Driver d in drivers)
            {
                d.Location.LastUpdated = DateTime.Now;
            }

            return drivers;
        }

        public static List<Location> temporaryTestLocations()
        {
            Location ones = new Location();
            ones.Latitude = 1.3333333;
            ones.Latitude_sign = "+";
            ones.Longitude = 1.3333333;
            ones.Longitude_sign = "+";
            Location twos = new Location();
            twos.Latitude = 2.44444;
            twos.Latitude_sign = "+";
            twos.Longitude = 2.44444;
            twos.Longitude_sign = "+";
            Location thre = new Location();
            thre.Latitude = 3.555555;
            thre.Latitude_sign = "+";
            thre.Longitude = 3.555555;
            thre.Longitude_sign = "+";
            Location four = new Location();
            four.Latitude = 4.666666;
            four.Latitude_sign = "+";
            four.Longitude = 4.666666;
            four.Longitude_sign = "+";
            Location five = new Location();
            five.Latitude = 5.777777;
            five.Latitude_sign = "+";
            five.Longitude = 5.777777;
            five.Longitude_sign = "+";

            List<Location> locations = new List<Location>()
            {
                ones,
                twos,
                thre,
                four,
                five
            };

            foreach (Location l in locations)
            {
                l.LastUpdated = DateTime.Now;
            }

            return locations;
        }

        public static List<FareRequest> temporaryTestFares()
        {
            List<Customer> customers = temporaryTestCustomers();
            List<Location> destinations = temporaryTestLocations();
            List<FareRequest> fares = new List<FareRequest>();

            int all = customers.Count - 1;

            for (int i = 0; i < customers.Count; i++)
            {
                FareRequest f = new FareRequest(customers[i], destinations[all - i]);
                fares.Add(f);
            }

            return fares;
        }

        public static List<FareRequest> buildJSONFares(List<FareRequest> fares)
        {
            List<FareRequest> listToReturn = new List<FareRequest>();

            foreach (FareRequest f in fares)
            {
                FareRequest fare = new FareRequest();
                fare.Customer = new Customer();
                fare.Customer.Location = new Location();
                fare.Destination = new Location();

                if (f.FareNumber != null)
                {
                    fare.FareNumber = f.FareNumber;
                }

                if (f.Customer != null)
                {
                    if (f.Customer.Location != null)
                    {
                        fare.Customer.Location.Latitude = f.Customer.Location.Latitude;
                        fare.Customer.Location.Latitude_sign = f.Customer.Location.Latitude_sign;
                        fare.Customer.Location.Longitude = f.Customer.Location.Longitude;
                        fare.Customer.Location.Longitude_sign = f.Customer.Location.Longitude_sign;
                    }
                }

                if (f.Destination != null)
                {
                    fare.Destination.Latitude = f.Destination.Latitude;
                    fare.Destination.Latitude_sign = f.Destination.Latitude_sign;
                    fare.Destination.Longitude = f.Destination.Longitude;
                    fare.Destination.Longitude_sign = f.Destination.Longitude_sign;
                }

                listToReturn.Add(fare);
            }

            return listToReturn;
        }

        public static List<Driver> buildJSONDrivers(List<Driver> drivers)
        {
            List<Driver> listToReturn = new List<Driver>();

            for(int i = 0; i < drivers.Count; i++)
            {
                Driver driver = new Driver();
                driver.Location = new Location();
                driver.UserID = drivers[i].UserID;
                Location checkLocation = drivers[i].Location;

                if (checkLocation != null)
                {
                    driver.Location.Latitude = drivers[i].Location.Latitude;
                    driver.Location.Latitude_sign = drivers[i].Location.Latitude_sign;
                    driver.Location.Longitude = drivers[i].Location.Longitude;
                    driver.Location.Longitude_sign = drivers[i].Location.Longitude_sign;
                }
                listToReturn.Add(driver);
            }

            return listToReturn;

        }

        public static List<Customer> buildJSONCustomers(List<Customer> customers)
        {
            List<Customer> listToReturn = new List<Customer>();

            for(int i = 0; i < customers.Count; i++)
            {
                Customer customer = new Customer();
                customer.Location = new Location();
                customer.UserID = customers[i].UserID;
                Location checkLocation = customers[i].Location;

                if (checkLocation != null)
                {
                    customer.Location.Latitude = customers[i].Location.Latitude;
                    customer.Location.Latitude_sign = customers[i].Location.Latitude_sign;
                    customer.Location.Longitude = customers[i].Location.Longitude;
                    customer.Location.Longitude_sign = customers[i].Location.Longitude_sign;
                }

                listToReturn.Add(customer);
            }

            return listToReturn;
        }
    }
}