﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestDispatch.Models
{
    public class FareRequest
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FareNumber { get; set; }

        [DefaultValue(false)]
        public Boolean assigned { get; set; }

        [ForeignKey("Customer_ID")]
        public virtual Customer Customer { get; set; }

        public int Customer_ID { get; set; }

        [ForeignKey("Destination_ID")]
        public virtual Location Destination { get; set; }

        public int Destination_ID { get; set; }

        public FareRequest(Customer customer, Location destination)
        {
            this.Customer = customer;
            this.Destination = destination;
        }

        public FareRequest()
        {

        }
    }
}