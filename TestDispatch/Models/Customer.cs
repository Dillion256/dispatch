﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestDispatch.Models
{
    public class Customer
    {
        [Key]
        public int UserID { get; set; }

        // This field should be used to store a unique identifier for the user's device.
        public string deviceID { get; set; }

        [ForeignKey("Location_ID")]
        public virtual Location Location { get; set; }

        public int Location_ID { get; set; }


        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        [ForeignKey("DriverRefID")]
        public virtual Driver CurrentDriver { get; set; }

        public int? DriverRefID { get; set; }



        public Customer()
        {

        }

        public Customer(Location location)
        {
            this.Location = location;
        }

        public Boolean hasAssignedDriver()
        {
            if (CurrentDriver == null)
            {
                return false;
            }

            return true;
        }
    }
}