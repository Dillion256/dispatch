﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestDispatch.Models
{
    public class Driver
    {
        [Key]
        public int UserID { get; set; }

        public string deviceID { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        [ForeignKey("Location_ID")]
        public virtual Location Location { get; set; }

        public int Location_ID { get; set; }

        public Driver(Location location)
        {
            this.Location = location;
            Customers = new List<Customer>();
        }

        public Driver()
        {
            Customers = new List<Customer>();
        }

        public void addCustomer(Customer customer)
        {
            if (Customers == null)
            {
                Customers = new List<Customer>();
            }

            Customers.Add(customer);
        }

        public Boolean hasCustomer()
        {
            if (Customers.Count > 0)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}