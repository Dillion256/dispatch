﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace TestDispatch.Models
{
    public class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        public double Longitude { get; set; }
        [Required]
        [RegularExpression(@"^[\+\-]{1}$")]
        public string Longitude_sign { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required]
        [RegularExpression(@"^[\+\-]{1}$")]
        public string Latitude_sign { get; set; }
        public DateTime? LastUpdated { get; set; }


    }

    public class LocationDataContext : DbContext
    {
        public LocationDataContext()
            : base("DefaultConnection")
        {

        }

        public DbSet<Location> Locations { get; set; }
        public DbSet<Driver> DriverLocations { get; set; }
        public DbSet<FareRequest> FareRequests { get; set; }
        public DbSet<Customer> ActiveCustomers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*modelBuilder.Entity<Customer>()
                .HasRequired(c => c.Location)
                .WithRequiredPrincipal();
            modelBuilder.Entity<FareRequest>()
                .HasRequired(f => f.Destination)
                .WithRequiredDependent()
                .WillCascadeOnDelete(false);*/
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}