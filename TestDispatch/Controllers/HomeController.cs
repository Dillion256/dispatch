﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using TestDispatch.Helpers;
using TestDispatch.Models;

namespace TestDispatch.Controllers
{
    public class HomeController : ApiController
    {
        private LocationDataContext db = new LocationDataContext();

        [HttpGet]
        public IHttpActionResult Index()
        {
            return Json("Greetings");
        }

        [HttpPost]
        public IHttpActionResult Reset()
        {
            deleteAllData();

            return Json("Data reset");
        }

        [HttpPut]
        public IHttpActionResult Reseed()
        {
            reseed();

            return Json("Data seeded");
        }

        public void deleteAllData()
        {
            foreach (Location l in db.Locations.ToList())
            {
                db.Locations.Remove(l);
                db.Entry(l).State = EntityState.Deleted;
            }

            foreach (Driver d in db.DriverLocations.ToList())
            {
                db.DriverLocations.Remove(d);
                db.Entry(d).State = EntityState.Deleted;
            }

            foreach (Customer c in db.ActiveCustomers.ToList())
            {
                db.ActiveCustomers.Remove(c);
                db.Entry(c).State = EntityState.Deleted;
            }

            foreach (FareRequest f in db.FareRequests.ToList())
            {
                db.FareRequests.Remove(f);
                db.Entry(f).State = EntityState.Deleted;
            }

            db.SaveChanges();
            return;
        }

        public void reseed()
        {
            List<Location> destinations = StaticMethods.temporaryTestLocations();
            List<Driver> drivers = StaticMethods.temporaryTestDrivers();
            List<Customer> customers = StaticMethods.temporaryTestCustomers();
            List<FareRequest> fares = StaticMethods.temporaryTestFares();

            foreach (Location l in destinations)
            {
                db.Locations.Add(l);
                db.Entry(l).State = EntityState.Added;
            }

            foreach (Driver d in drivers)
            {
                Location l = new Location();
                l.Latitude_sign = d.Location.Latitude_sign;
                l.Latitude = d.Location.Latitude;
                l.Longitude = d.Location.Longitude;
                l.Longitude_sign = d.Location.Longitude_sign;
                l.LastUpdated = DateTime.Now;
                db.Locations.Add(l);
                db.Entry(l).State = EntityState.Added;
                d.Location = l;
                db.DriverLocations.Add(d);
                db.Entry(d).State = EntityState.Added;
            }

            foreach (Customer c in customers)
            {
                Location l = new Location();
                l.Latitude_sign = c.Location.Latitude_sign;
                l.Latitude = c.Location.Latitude;
                l.Longitude = c.Location.Longitude;
                l.Longitude_sign = c.Location.Longitude_sign;
                l.LastUpdated = DateTime.Now;
                db.Locations.Add(l);
                db.Entry(c.Location).State = EntityState.Added;
                c.Location = l;
                db.ActiveCustomers.Add(c);
                db.Entry(c).State = EntityState.Added;
            }

            db.SaveChanges();
            return;
        }
    }
}