﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDispatch.Helpers;
using TestDispatch.Models;

namespace TestDispatch.Controllers
{
    public class FareRequestController : ApiController
    {
        private LocationDataContext db = new LocationDataContext();

        [HttpGet]
        public IHttpActionResult AllFares()
        {
            List<FareRequest> allFares = db.FareRequests.ToList();

            if (allFares == null)
            {
                allFares = StaticMethods.temporaryTestFares();
            }

            if (allFares.Count < 1)
            {
                allFares = StaticMethods.temporaryTestFares();
            }

            allFares = StaticMethods.buildJSONFares(allFares);

            return Json(allFares);
        }

        [HttpGet]
        public IHttpActionResult GetFare(int fareID)
        {
            FareRequest fare = db.FareRequests.Find(fareID);

            if (fare == null)
            {
                return Json("Invalid fare ID");
            }

            List<FareRequest> listToPass = new List<FareRequest>();
            listToPass.Add(fare);
            listToPass = StaticMethods.buildJSONFares(listToPass);

            return Json(listToPass);
        }

        [HttpGet]
        public IHttpActionResult CustomersBy(int driverID, string sortParameter)
        {
            Driver driver = db.DriverLocations.Find(driverID);
            List<FareRequest> fares = db.FareRequests.ToList();
            List<Customer> customers = new List<Customer>();
            List<FareRequest> driversFares = new List<FareRequest>();

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            foreach (FareRequest fare in fares)
            {
                customers.Add(fare.Customer);

                if (driver.Customers.Contains(fare.Customer))
                {
                    driversFares.Add(fare);
                }
            }

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            if (sortParameter == null)
            {
                return Json(StaticMethods.buildJSONFares(fares));
            }

            if (sortParameter.Equals("location"))
            {
                return Json(StaticMethods.buildJSONCustomers(StaticMethods.getClosestCustomers(driver, customers)));
            }

            /*if (sortParameter.Equals("heading"))
            {
                return Json(StaticMethods.getCustomersByHeading(driver, StaticMethods.getFarthestCustomer(driver, driversFares).Destination, fares));
            }*/

            return Json("Invalid sort parameter");
        }

        [HttpGet]
        public IHttpActionResult AllEmptyDrivers([FromUri] Location location)
        {
            if (ModelState.IsValid == false)
            {
                return Json("Invalid location information. You must sign up first.");
            }

            
            List<Driver> listToReturn = new List<Driver>();
            List<Driver> drivers = db.DriverLocations.ToList();
            foreach (Driver d in drivers)
            {
                Boolean isValidDriver = false;

                if (d.Customers == null)
                {
                    isValidDriver = true;
                }

                else if (d.Customers.Count == 0)
                {
                    isValidDriver = true;
                }

                if (d.Location != null)
                {
                    if (StaticMethods.checkIfCurrent(d.Location.LastUpdated) == false)
                    {
                        isValidDriver = false;
                    }

                    if (StaticMethods.distanceFrom(d.Location, location) > 1.0)
                    {
                        isValidDriver = false;
                    }
                }

                if (isValidDriver == true)
                {
                    listToReturn.Add(d);
                }
            }

            return Json(StaticMethods.buildJSONDrivers(listToReturn));

        }

        [HttpPost]
        public IHttpActionResult RequestFare([FromBody] FareRequest fare)
        {
            if (ModelState.IsValid == false)
            {
                return Json("Invalid fare data");
            }

            if (fare.Customer == null)
            {
                return Json("Incomplete fare information. Please include customer information.");
            }

            if (fare.Customer.Location == null)
            {
                return Json("Incomplete customer data. Please include your location.");
            }

            if (fare.Destination == null)
            {
                return Json("Incomplete fare information. Please include a destination");
            }

            if(fare.Customer_ID != 0 && fare.Customer.UserID != 0 && fare.Customer_ID != fare.Customer.UserID)
            {
                return Json("Do not send customer ID foreign key");
            }

            if (fare.FareNumber != 0)
            {
                return Json("Please do not send fare ID");
            }

            Customer checkCustomer = db.ActiveCustomers.Find(fare.Customer.UserID);

            if (checkCustomer != null)
            {
                foreach(FareRequest f in db.FareRequests.ToList())
                {
                    if(f.Customer_ID == checkCustomer.UserID)
                    {
                        return Json("You already have a fare pending. If you wish to request a new ride, please cancel that request. If you wish to checnge your destination, please call UpdateFare.");
                    }
                }
                fare.Customer = checkCustomer;
            }

            else
            {
                Customer newCustomer = fare.Customer;
                newCustomer.UserID = fare.Customer.UserID;
                db.ActiveCustomers.Add(fare.Customer);
                fare.Customer.Location.LastUpdated = DateTime.Now;
                db.Locations.Add(fare.Customer.Location);
            }

            db.FareRequests.Add(fare);
            db.Entry(fare).State = EntityState.Added;
            db.SaveChanges();

            List<FareRequest> listToPass = new List<FareRequest>();
            listToPass.Add(fare);

            return Json(StaticMethods.buildJSONFares(listToPass));

        }

        [HttpPatch]
        public IHttpActionResult UpdateFare([FromBody] FareRequest fare, int fareID)
        {
            if (ModelState.IsValid == false)
            {
                return Json("Invalid fare information");
            }

            FareRequest checkFare = db.FareRequests.Find(fareID);

            if (checkFare == null)
            {
                return Json("Fare not found in database");
            }

            if (fare.FareNumber != 0)
            {
                if (fare.FareNumber != fareID)
                {
                    return Json("Inconsistent IDs");
                }
            }

            if(fare.Customer != null)
            {
                if (fare.Customer.UserID != checkFare.Customer_ID)
                {
                    return Json("Inconsistent customer IDs");
                }
            }

            if (fare.Customer_ID != checkFare.Customer_ID && fare.Customer_ID != 0)
            {
                return Json("Inconsistent customer IDs");
            }

            if (fare.Destination != null)
            {
                checkFare.Destination.Latitude = fare.Destination.Latitude;
                checkFare.Destination.Latitude_sign = fare.Destination.Latitude_sign;
                checkFare.Destination.Longitude = fare.Destination.Longitude;
                checkFare.Destination.Longitude_sign = fare.Destination.Longitude_sign;
                checkFare.Destination.LastUpdated = DateTime.Now;
                db.Entry(checkFare.Destination).State = EntityState.Modified;
            }

            if (fare.Customer != null)
            {
                if (fare.Customer.Location != null)
                {
                    checkFare.Customer.Location.Latitude = fare.Customer.Location.Latitude;
                    checkFare.Customer.Location.Latitude_sign = fare.Customer.Location.Latitude_sign;
                    checkFare.Customer.Location.Longitude = fare.Customer.Location.Longitude;
                    checkFare.Customer.Location.Longitude_sign = fare.Customer.Location.Longitude_sign;
                    checkFare.Customer.Location.LastUpdated = DateTime.Now;
                    db.Entry(checkFare.Customer.Location).State = EntityState.Modified;
                }
            }

            db.SaveChanges();

            List<FareRequest> listToPass = new List<FareRequest>();
            listToPass.Add(checkFare);

            return Json(StaticMethods.buildJSONFares(listToPass));
        }

        [HttpPatch]
        public IHttpActionResult AssignFare(int fareID, int driverID)
        {
            FareRequest fare = db.FareRequests.Find(fareID);
            Driver driver = db.DriverLocations.Find(driverID);

            if (fare == null)
            {
                return Json("Fare not found in database");
            }

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            if (fare.Customer_ID == 0)
            {
                db.FareRequests.Remove(fare);
                db.Entry(fare).State = EntityState.Deleted;
                db.SaveChanges();
                return Json("Fare was invalid. It was deleted from the database");
            }

            Customer customer = db.ActiveCustomers.Find(fare.Customer_ID);

            if (customer == null)
            {
                db.FareRequests.Remove(fare);
                db.Entry(fare).State = EntityState.Deleted;
                db.SaveChanges();
                return Json("Customer was invalid. It was deleted from the database");
            }

            customer.CurrentDriver = driver;
            driver.Customers.Add(customer);
            fare.assigned = true;
            db.Entry(customer).State = EntityState.Modified;
            db.Entry(driver).State = EntityState.Modified;
            db.Entry(fare).State = EntityState.Modified;

            db.SaveChanges();

            List<FareRequest> listToPass = new List<FareRequest>();
            listToPass.Add(fare);
            listToPass = StaticMethods.buildJSONFares(listToPass);
            
            return Json(listToPass);
        }

        [HttpDelete]
        public IHttpActionResult CancelFare(int fareID)
        {
            FareRequest fare = db.FareRequests.Find(fareID);

            if (fare == null)
            {
                return Json("Invalid fare ID");
            }

            db.FareRequests.Remove(fare);
            db.Entry(fare).State = EntityState.Deleted;

            db.SaveChanges();

            return Json("Fare deleted");
        }

        

        // Needs testing
        

        

        public void UpdateDriverQueue(Driver driver)
        {
            Boolean checkedOut = false;
            if (driver.Location.LastUpdated == null)
            {
                checkedOut = true;
            }
            if (checkedOut == false)
            {
                TimeSpan driverSpan = DateTime.Now - driver.Location.LastUpdated.Value;
                if (driverSpan.Minutes > 60)
                {
                    checkedOut = true;
                }
            }

            if (checkedOut == true)
            {
                driver.Customers = new List<Customer>();
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return;
            }

            foreach (Customer customer in driver.Customers)
            {
                if (customer.Location.LastUpdated == null)
                {
                    driver.Customers.Remove(customer);
                }
                else
                {
                    TimeSpan customerSpan = DateTime.Now - customer.Location.LastUpdated.Value;
                    if (Helpers.StaticMethods.checkIfCurrent(customer.Location.LastUpdated) == false)
                    {
                        driver.Customers.Remove(customer);
                    }
                }
            }

            db.Entry(driver).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}