﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDispatch.Helpers;
using TestDispatch.Models;

namespace TestDispatch.Controllers
{
    public class LocationController : ApiController
    {
        private LocationDataContext db = new LocationDataContext();

        [HttpGet]
        public IHttpActionResult AllLocations()
        {
            List<Location> allLocations = db.Locations.ToList();

            if (allLocations == null)
            {
                allLocations = StaticMethods.temporaryTestLocations();
            }

            if (allLocations.Count < 1)
            {
                allLocations = StaticMethods.temporaryTestLocations();
            }

            return Json(allLocations);
        }

    }
}
