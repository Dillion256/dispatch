﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDispatch.Helpers;
using TestDispatch.Models;

namespace TestDispatch.Controllers
{
    public class DriverController : ApiController
    {
        private LocationDataContext db = new LocationDataContext();

        [HttpGet]
        public IHttpActionResult AllDrivers()
        {
            List<Driver> allDrivers = db.DriverLocations.ToList();
            if (allDrivers.Count < 1)
            {
                allDrivers = StaticMethods.temporaryTestDrivers();
            }

            return Json(StaticMethods.buildJSONDrivers(allDrivers));
        }

        [HttpGet]
        public IHttpActionResult GetDriverLocation(int driverID)
        {
            Driver driver = db.DriverLocations.Find(driverID);

            if(driver == null)
            {
                return Json("Invalid driver ID");
            }

            List<Driver> listToPass = new List<Driver>();
            listToPass.Add(driver);


            return Json(StaticMethods.buildJSONDrivers(listToPass));
        }

        [HttpGet]
        public IHttpActionResult GetDriverWithDevice(string deviceID)
        {
            List<Driver> listToPass = new List<Driver>();
            Boolean found = false;
            foreach (Driver d in db.DriverLocations)
            {
                if (d.deviceID.Equals(deviceID))
                {
                    listToPass.Add(d);
                    found = true;
                    break;
                }
                
            }

            if (found == true)
            {
                return Json(StaticMethods.buildJSONDrivers(listToPass));
            }
            
            return Json("Could not find device ID in database");
        }

        [HttpGet]
        public IHttpActionResult GetCustomerLocations(int driverID, string orderedBy)
        {
            Driver driver = db.DriverLocations.Find(driverID);

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            UpdateDriverQueue(driver);

            if (driver.Customers != null && driver.Customers.Count > 0)
            {
                return Json(StaticMethods.buildJSONCustomers(driver.Customers.ToList()));
            }

            if (driver.Customers.Count == 0)
            {
                return Json(StaticMethods.temporaryTestCustomers());
            }

            return Json("Driver currently has no customers");
        }

        [HttpPost]
        public IHttpActionResult CreateDriver([FromBody] Driver driver)
        {
            if (ModelState.IsValid)
            {
                Driver checkIfExists = db.DriverLocations.Find(driver.UserID);
                if (checkIfExists != null)
                {
                    return Json("ID already exists");
                }

                if (driver.Location == null || driver.deviceID == null)
                {
                    return Json("Please include your location and device ID.");
                }

                driver.Location.LastUpdated = DateTime.Now;
                db.DriverLocations.Add(driver);
                db.Locations.Add(driver.Location);
                db.SaveChanges();

                List<Driver> listToPass = new List<Driver>();
                listToPass.Add(driver);

                return Json(StaticMethods.buildJSONDrivers(listToPass));
            }

            return Json("Invalid driver data");
        }

        [HttpPost]
        public IHttpActionResult RejectFare(int driverID, [FromBody] FareRequest fare)
        {
            if (ModelState.IsValid == false)
            {
                return Json("Invalid fare object");
            }

            if (fare.FareNumber == 0)
            {
                return Json("No fare number");
            }

            if (fare.Customer_ID == 0)
            {
                return Json("No customer set to fare");
            }

            Driver driver = db.DriverLocations.Find(driverID);
            Customer customer = db.ActiveCustomers.Find(fare.Customer_ID);
            FareRequest fareCheck = db.FareRequests.Find(fare.FareNumber);

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            UpdateDriverQueue(driver);

            if (customer == null)
            {
                foreach (Customer c in driver.Customers)
                {
                    if (c.UserID == fare.Customer_ID)
                    {
                        driver.Customers.Remove(c);
                        db.Entry(driver).State = EntityState.Modified;
                        db.SaveChanges();
                        break;
                    }
                }
                return Json("Customer information is invalid; customer has been removed from you queue");
            }

            if (fareCheck == null)
            {
                return Json("Invalid fare ID");
            }

            foreach (Customer c in driver.Customers)
            {
                if (c == customer)
                {
                    driver.Customers.Remove(customer);
                    customer.CurrentDriver = null;
                    fareCheck.assigned = false;
                    db.Entry(fareCheck).State = EntityState.Modified;
                    db.Entry(driver).State = EntityState.Modified;
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("Customer removed");
                }
            }

            return Json("Could not find customer in your queue");
        }

        

        [HttpPut]
        public IHttpActionResult UpdateDriver([FromBody] Driver driver)
        {
            if (ModelState.IsValid)
            {
                Driver oldDriver = db.DriverLocations.Find(driver.UserID);
                Location driverLocation = null;

                if (oldDriver == null)
                {
                    return Json("Could not find driver ID in database.");
                }

                UpdateDriverQueue(oldDriver);

                if (oldDriver.Location_ID != 0)
                {
                    driverLocation = db.Locations.Find(oldDriver.Location_ID);
                }


                if (driverLocation != null && driver.Location != null)
                {
                    driverLocation.Latitude = driver.Location.Latitude;
                    driverLocation.Latitude_sign = driver.Location.Latitude_sign;
                    driverLocation.Longitude = driver.Location.Longitude;
                    driverLocation.Longitude_sign = driver.Location.Longitude_sign;
                    driverLocation.LastUpdated = DateTime.Now;
                    db.Entry(driverLocation).State = EntityState.Modified;
                }

                if (driver.Location == null)
                {
                    return Json("Please include location data for update");
                }

                db.Entry(oldDriver).State = EntityState.Modified;
                db.SaveChanges();

                List<Driver> listToPass = new List<Driver>();
                listToPass.Add(oldDriver);

                return Json(StaticMethods.buildJSONDrivers(listToPass));
            }



            return Json("Invalid driver data");
        }

        [HttpPatch]
        public IHttpActionResult PatchDriver([FromBody] Driver driver)
        {
            if (ModelState.IsValid)
            {
                Driver oldDriver = db.DriverLocations.Find(driver.UserID);
                Location driverLocation = null;

                if (oldDriver == null)
                {
                    return Json("Could not find driver ID in database.");
                }

                UpdateDriverQueue(oldDriver);

                if (oldDriver.Location_ID != 0)
                {
                    driverLocation = db.Locations.Find(oldDriver.Location_ID);
                }

                if (driver.deviceID != null)
                {
                    oldDriver.deviceID = driver.deviceID;
                    db.Entry(oldDriver).State = EntityState.Modified;
                }


                if (driverLocation != null && driver.Location != null)
                {
                    driverLocation.Latitude = driver.Location.Latitude;
                    driverLocation.Latitude_sign = driver.Location.Latitude_sign;
                    driverLocation.Longitude = driver.Location.Longitude;
                    driverLocation.Longitude_sign = driver.Location.Longitude_sign;
                    driverLocation.LastUpdated = DateTime.Now;
                    db.Entry(driverLocation).State = EntityState.Modified;
                }

                if (driver.Location == null)
                {
                    return Json("Please include location data for update");
                }

                db.Entry(oldDriver).State = EntityState.Modified;
                db.SaveChanges();

                List<Driver> listToPass = new List<Driver>();
                listToPass.Add(oldDriver);

                return Json(StaticMethods.buildJSONDrivers(listToPass));
            }



            return Json("Invalid driver data");
        }

        // Needs testing
        [HttpPatch]
        public IHttpActionResult AcceptFare(int driverID, int customerID)
        {
            Driver driver = db.DriverLocations.Find(driverID);
            FareRequest fare = null;
            Customer customer = null;

            Boolean fareWasFound = false;

            foreach (FareRequest f in db.FareRequests)
            {
                if (f.Customer_ID == customerID)
                {
                    fare = f;
                    fareWasFound = true;
                    break;
                }
            }

            if (fareWasFound == true)
            {
                customer = fare.Customer;
            }

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            UpdateDriverQueue(driver);

            if (fare == null)
            {
                return Json("Invalid customer ID, or customer has no requested fares");
            }

            driver.addCustomer(fare.Customer);
            fare.Customer.CurrentDriver = driver;
            fare.assigned = true;
            db.Entry(driver).State = EntityState.Modified;
            db.Entry(customer).State = EntityState.Modified;
            db.Entry(fare).State = EntityState.Modified;

            db.SaveChanges();

            List<FareRequest> listToPass = new List<FareRequest>();
            listToPass.Add(fare);

            return Json(StaticMethods.buildJSONFares(listToPass));

        }

        

        [HttpPatch]
        public IHttpActionResult Dropoff(int driverID, [FromBody] Customer customer)
        {
            Driver driver = db.DriverLocations.Find(driverID);

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            if (ModelState.IsValid == false)
            {
                return Json("Invalid customer data");
            }

            if (customer.UserID == 0)
            {
                return Json("Invalid customer iD");
            }

            Customer checkCustomer = db.ActiveCustomers.Find(customer.UserID);

            if (checkCustomer == null)
            {
                Customer driverCheckCustomer = null;
                foreach (Customer c in driver.Customers.ToList())
                {
                    if (c.UserID == checkCustomer.UserID)
                    {
                        driverCheckCustomer = c;
                        break;
                    }
                }
                driver.Customers.Remove(driverCheckCustomer);
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return Json("Could not find customer in database. Customer has been removed from your queue.");
            }

            checkCustomer.CurrentDriver = null;
            db.Entry(checkCustomer).State = EntityState.Modified;

            Customer customerToRemove = null;
            
            foreach (Customer c in driver.Customers.ToList())
            {
                if (c.UserID == checkCustomer.UserID)
                {
                    customerToRemove = c;
                    break;
                }
            }

            if (customerToRemove != null)
            {
                driver.Customers.Remove(customerToRemove);
                UpdateDriverQueue(driver);
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return Json("Removed customer from your queue");
            }

            UpdateDriverQueue(driver);
            return Json("Could not find customer in your queue.");
        }

        [HttpDelete]
        public IHttpActionResult FireDriver(int driverID)
        {
            Driver driver = db.DriverLocations.Find(driverID);

            if (driver == null)
            {
                return Json("Invalid driver ID");
            }

            db.DriverLocations.Remove(driver);
            db.Entry(driver).State = EntityState.Deleted;
            db.SaveChanges();

            return Json("Deleted");
        }

        public void UpdateDriverQueue(Driver driver)
        {
            Boolean checkedOut = false;
            if (driver.Location.LastUpdated == null)
            {
                checkedOut = true;
            }
            if (checkedOut == false)
            {
                TimeSpan driverSpan = DateTime.Now - driver.Location.LastUpdated.Value;
                if (driverSpan.Minutes > 60)
                {
                    checkedOut = true;
                }
            }

            if (checkedOut == true)
            {
                driver.Customers = new List<Customer>();
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return;
            }

            foreach (Customer customer in driver.Customers.ToList())
            {
                if (customer.Location.LastUpdated == null)
                {
                    driver.Customers.Remove(customer);
                }
                else
                {
                    TimeSpan customerSpan = DateTime.Now - customer.Location.LastUpdated.Value;
                    if (Helpers.StaticMethods.checkIfCurrent(customer.Location.LastUpdated) == false)
                    {
                        driver.Customers.Remove(customer);
                    }
                }
            }

            db.Entry(driver).State = EntityState.Modified;
            db.SaveChanges();
        }

        [HttpGet]
        public IHttpActionResult Test()
        {
            return Json("Success");
        }
    }
}