﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDispatch.Helpers;
using TestDispatch.Models;

namespace TestDispatch.Controllers
{
    public class CustomerController : ApiController
    {
        private LocationDataContext db = new LocationDataContext();

        [HttpGet]
        public IHttpActionResult AllCustomers()
        {
            List<Customer> allCustomers = db.ActiveCustomers.ToList();

            if (allCustomers == null)
            {
                return Json(StaticMethods.temporaryTestCustomers());
            }

            if (allCustomers.Count < 1)
            {
                return Json(StaticMethods.temporaryTestCustomers());
            }

            return Json(StaticMethods.buildJSONCustomers(allCustomers));
        }

        [HttpGet]
        public IHttpActionResult GetCustomer(int customerID)
        {
            Customer customer = db.ActiveCustomers.Find(customerID);

            if (customer == null)
            {
                return Json("Invalid customer ID");
            }

            List<Customer> listToPass = new List<Customer>();
            listToPass.Add(customer);

            return Json(StaticMethods.buildJSONCustomers(listToPass));
        }

        [HttpGet]
        public IHttpActionResult GetCustomerWithDevice(string deviceID)
        {
            List<Customer> listToPass = new List<Customer>();
            Boolean found = false;
            foreach (Customer c in db.ActiveCustomers)
            {
                if(c.deviceID.Equals(deviceID))
                {
                    listToPass.Add(c);
                    found = true;
                    break;
                }
            }

            if (found == true)
            {
                return Json(StaticMethods.buildJSONCustomers(listToPass));
            }

            return Json("Could not find device ID in database");
        }

        [HttpPost]
        public IHttpActionResult CreateCustomer([FromBody] Customer customer)
        {

            if (ModelState.IsValid)
            {
                Customer checkIfExists = db.ActiveCustomers.Find(customer.UserID);
                if (checkIfExists != null)
                {
                    return Json("ID already exists.");
                }

                if (customer.Location == null || customer.deviceID == null)
                {
                    return Json("Please include your location and device ID.");
                }

                customer.Location.LastUpdated = DateTime.Now;
                db.Locations.Add(customer.Location);
                db.ActiveCustomers.Add(customer);
                db.SaveChanges();

                List<Customer> listToPass = new List<Customer>();
                listToPass.Add(customer);

                return Json(StaticMethods.buildJSONCustomers(listToPass));
            }

            return Json("Invalid customer data.");
        }

        [HttpPut]
        public IHttpActionResult UpdateCustomer([FromBody] Customer customer)
        {
            if (ModelState.IsValid)
            {
                Customer oldCustomer = db.ActiveCustomers.Find(customer.UserID);
                Location customerLocation = null;

                if (oldCustomer == null)
                {
                    return Json("Could not find user ID in database.");
                }

                if (customer.Email == null || customer.PhoneNumber == null || customer.Location == null)
                {
                    return Json("Please include your phone number, email, and location. If you wish to update only part of your account information, use the PatchCustomer method");
                }

                if (customer.DriverRefID != null || customer.CurrentDriver != null || customer.Location_ID != 0)
                {
                    return Json("Only use this URL to update your account information. To request a call or update your fare request, please use the FareRequest controller");
                }

                if(oldCustomer.Location_ID != 0)
                {
                    customerLocation = db.Locations.Find(oldCustomer.Location_ID);
                }

                oldCustomer.Email = customer.Email;
                oldCustomer.PhoneNumber = customer.PhoneNumber;

                if (customerLocation != null && customer.Location != null)
                {
                    customerLocation.Latitude = customer.Location.Latitude;
                    customerLocation.Latitude_sign = customer.Location.Latitude_sign;
                    customerLocation.Longitude = customer.Location.Longitude;
                    customerLocation.Longitude_sign = customer.Location.Longitude_sign;
                    customerLocation.LastUpdated = DateTime.Now;
                    db.Entry(customerLocation).State = EntityState.Modified;
                }

                db.Entry(oldCustomer).State = EntityState.Modified;
                db.SaveChanges();

                List<Customer> listToPass = new List<Customer>();
                listToPass.Add(oldCustomer);

                return Json(StaticMethods.buildJSONCustomers(listToPass));
            }

            

            return Json("Invalid customer data");
        }

        [HttpPatch]
        public IHttpActionResult PatchCustomer([FromBody] Customer customer)
        {
            if (ModelState.IsValid == false)
            {
                return Json("Invalid customer data");
            }

            if (customer.DriverRefID != null || customer.CurrentDriver != null || customer.Location_ID != 0)
            {
                return Json("Only use this URL to update your account information. To request a call or update your fare request, please use the FareRequest controller");
            }
            Customer oldCustomer = db.ActiveCustomers.Find(customer.UserID);

            if (oldCustomer == null)
            {
                return Json("Invalid customer ID");
            }


            if (oldCustomer.Location_ID != 0 && customer.Location != null)
            {
                Location location = db.Locations.Find(oldCustomer.Location_ID);
                if (location != null)
                {
                    location.Latitude = customer.Location.Latitude;
                    location.Latitude_sign = customer.Location.Latitude_sign;
                    location.Longitude = customer.Location.Longitude;
                    location.Longitude_sign = customer.Location.Longitude_sign;
                    location.LastUpdated = DateTime.Now;
                    db.Entry(location).State = EntityState.Modified;
                }

                else
                {
                    db.Locations.Add(customer.Location);
                    oldCustomer.Location = customer.Location;
                    db.Entry(customer).State = EntityState.Modified;
                }
            }

            if (customer.Email != null)
            {
                oldCustomer.Email = customer.Email;
                db.Entry(oldCustomer).State = EntityState.Modified;
            }

            if (customer.PhoneNumber != null)
            {
                oldCustomer.PhoneNumber = customer.PhoneNumber;
                db.Entry(oldCustomer).State = EntityState.Modified;
            }

            db.SaveChanges();

            List<Customer> listToPass = new List<Customer>();
            listToPass.Add(oldCustomer);

            return Json(StaticMethods.buildJSONCustomers(listToPass));
        }

        [HttpDelete]
        public IHttpActionResult DeleteCustomer(int customerID)
        {
            Customer customer = db.ActiveCustomers.Find(customerID);

            if (customer == null)
            {
                return Json("Invalid customer ID");
            }

            db.ActiveCustomers.Remove(customer);
            db.Entry(customer).State = EntityState.Deleted;
            db.SaveChanges();

            return Json("Deleted");
        }

        public void UpdateDriverQueue(Driver driver)
        {
            Boolean checkedOut = false;
            if (driver.Location.LastUpdated == null)
            {
                checkedOut = true;
            }
            if (checkedOut == false)
            {
                TimeSpan driverSpan = DateTime.Now - driver.Location.LastUpdated.Value;
                if (driverSpan.Minutes > 60)
                {
                    checkedOut = true;
                }
            }

            if (checkedOut == true)
            {
                driver.Customers = new List<Customer>();
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return;
            }

            foreach (Customer customer in driver.Customers)
            {
                if (customer.Location.LastUpdated == null)
                {
                    driver.Customers.Remove(customer);
                }
                else
                {
                    TimeSpan customerSpan = DateTime.Now - customer.Location.LastUpdated.Value;
                    if (Helpers.StaticMethods.checkIfCurrent(customer.Location.LastUpdated) == false)
                    {
                        driver.Customers.Remove(customer);
                    }
                }
            }

            db.Entry(driver).State = EntityState.Modified;
            db.SaveChanges();
        }

        
    }
}