namespace TestDispatch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Assigned : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FareRequests", "assigned", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FareRequests", "assigned");
        }
    }
}
