namespace TestDispatch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "deviceID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "DeviceID", c => c.String());
        }
    }
}
