namespace TestDispatch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "DeviceID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "DeviceID");
        }
    }
}
