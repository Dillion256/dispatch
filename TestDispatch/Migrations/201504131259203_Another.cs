namespace TestDispatch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Another : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Longitude = c.Double(nullable: false),
                        Longitude_sign = c.String(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Latitude_sign = c.String(nullable: false),
                        LastUpdated = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Location_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.Locations", t => t.Location_ID)
                .Index(t => t.Location_ID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Location_ID = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        DriverRefID = c.Int(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.Locations", t => t.Location_ID)
                .ForeignKey("dbo.Drivers", t => t.DriverRefID)
                .Index(t => t.Location_ID)
                .Index(t => t.DriverRefID);
            
            CreateTable(
                "dbo.FareRequests",
                c => new
                    {
                        FareNumber = c.Int(nullable: false, identity: true),
                        Customer_ID = c.Int(nullable: false),
                        Destination_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FareNumber)
                .ForeignKey("dbo.Customers", t => t.Customer_ID)
                .ForeignKey("dbo.Locations", t => t.Destination_ID)
                .Index(t => t.Customer_ID)
                .Index(t => t.Destination_ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.FareRequests", new[] { "Destination_ID" });
            DropIndex("dbo.FareRequests", new[] { "Customer_ID" });
            DropIndex("dbo.Customers", new[] { "DriverRefID" });
            DropIndex("dbo.Customers", new[] { "Location_ID" });
            DropIndex("dbo.Drivers", new[] { "Location_ID" });
            DropForeignKey("dbo.FareRequests", "Destination_ID", "dbo.Locations");
            DropForeignKey("dbo.FareRequests", "Customer_ID", "dbo.Customers");
            DropForeignKey("dbo.Customers", "DriverRefID", "dbo.Drivers");
            DropForeignKey("dbo.Customers", "Location_ID", "dbo.Locations");
            DropForeignKey("dbo.Drivers", "Location_ID", "dbo.Locations");
            DropTable("dbo.FareRequests");
            DropTable("dbo.Customers");
            DropTable("dbo.Drivers");
            DropTable("dbo.Locations");
        }
    }
}
