namespace TestDispatch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeviceKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Drivers", "deviceID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Drivers", "deviceID");
        }
    }
}
